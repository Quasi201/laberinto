/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;
import Negocio.Laberinto;
/**
 *
 * @author Quasi
 */
public class Prueba {
    public static void main(String[] args) {
        Laberinto  l = new Laberinto("https://gitlab.com/santiagofelipeavil/archivos-persistencia/-/raw/master/laberinto/matrizLaberintoTeseo.txt");
        System.out.println("Matriz Antes del Recorrido:\n" + l.toString());
        String x = l.getCamino(l.teseo.getFirst(), l.teseo.getSecond(), l.minotauro.getFirst(), l.minotauro.getSecond(), l.salida.getFirst(), l.salida.getSecond());
        System.out.println("Matriz Despues del Recorrido:\n" + l.toString() + "\n" + x);
    }
}

