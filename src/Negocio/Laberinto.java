/**
 *
 * @author Quasi
 *         Harold Rueda
 */
package Negocio;
import Modelo.Pair;
import Util.*;

public class Laberinto {
    private short laberinto[][];
    private String path[][]; 
    private Cola<Pair<Integer, Integer>> cola = new Cola();
    public Pair<Integer, Integer> teseo, minotauro, salida, movimientos[];
    
    public Laberinto(String url){
        this.cargarDatos(url);
    }


    /**
    En las indicaciones se indica que este metodo debera ser recursivo
    pero la recursion se hace en otro metodo el cual se llama "recorrer"
    entonces, ¿hacerlo de esta manera estaria bien?
    */
    public String getCamino(int filaTeseo, int colTeseo, int filaM, int colM, int filaSalida, int colSalida){
        try {
            this.teseoToMinutauro(filaTeseo, colTeseo, filaM, colM); //Exactamente estos 2 metodos llaman a la recursion 
            this.minotauroToSalida(filaSalida, colSalida);           //para buscar el camino :)
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        if(path[filaSalida][colSalida].equals(""))return "No existe salida";
        return "Camino de Theseus: " + path[filaSalida][colSalida];
    }
    
    private void teseoToMinutauro(int filaTeseo, int colTeseo, int filaM, int colM)throws Exception{
        path[filaTeseo][colTeseo] = "("+filaTeseo+","+colTeseo+")";
        this.cola.enColar(teseo);
        this.recorrer(filaM, colM, false);
        cola.vaciar();
        if(path[filaM][colM].equals(""))
            throw new Exception("No es posible matar a el minotauro");
        this.limpiarMatriz(path[filaM][colM]);
    }
    
    private void minotauroToSalida(int filaSalida, int colSalida)throws Exception{
        this.cola.enColar(this.minotauro);
        recorrer(filaSalida, colSalida, true);
        cola.vaciar();
        if(path[filaSalida][colSalida].equals(""))
            throw new Exception("No es posible llegar a la salida, pero fue posible matar al minotauro");
        this.limpiarMatriz(path[filaSalida][colSalida]);
    }
    
    
    //Metodo Recursivo que busca el camino minimo
    private void recorrer(int filaFin, int columnaFin, boolean esSalida){
        if(!path[filaFin][columnaFin].equals("")) return;
        if(cola.esVacia())return;
        Pair<Integer,Integer> tmp = cola.deColar();
        for(Pair<Integer, Integer> p : this.movimientos) {
            int i=tmp.getFirst() + p.getFirst();
            int j=tmp.getSecond() + p.getSecond();
            if(i>=0 && i<laberinto.length && j>=0 && j<laberinto[0].length){
                if(laberinto[i][j] == 0){
                    cola.enColar(new Pair(i, j));
                    laberinto[i][j] = 1;
                    path[i][j] = path[tmp.getFirst()][tmp.getSecond()]+"-"+"("+i+","+j+")";
                }
                if(esSalida==false && laberinto[i][j] == 100){
                    cola.enColar(new Pair(i, j));
                    laberinto[i][j] = -100;
                    path[i][j] = path[tmp.getFirst()][tmp.getSecond()]+"-"+"("+i+","+j+")";
                }
                else if(esSalida && laberinto[i][j] == 1000)
                    path[i][j] = path[tmp.getFirst()][tmp.getSecond()]+"-"+"("+i+","+j+")";
            }    
        }
        recorrer(filaFin, columnaFin, esSalida);
    }
    
    private void limpiarMatriz(String ruta){
        for (int i = 0; i < this.laberinto.length; i++) {
            for (int j = 0; j <this.laberinto[0].length; j++) {
                if(this.laberinto[i][j] == 1)this.laberinto[i][j] = 0;
            }
        }
        for(int i=1, j=i+2; i<=ruta.length()-9; i+=6, j=i+2){
            int x = ruta.charAt(i)-'0', y = ruta.charAt(j)-'0';
            if(laberinto[x][y] == 0)laberinto[x][y] = 1;
        }
    }
    
    private void cargarDatos(String url){
        ArchivoLeerURL file = new ArchivoLeerURL(url);
        Object v[] = file.leerArchivo();
        int f = v.length;                         
        int c = v[0].toString().split(";").length; 
        laberinto =  new short[f][c];              
        path = new String[f][c];
        for (Integer i = 0; i < f; i++) {
            String fila[] = v[i].toString().split(";");
            for(Integer j =0; j<c; j++){
                short data =Short.parseShort(fila[j]);
                this.laberinto[i][j] = data;
                this.path[i][j] = "";
                if(data==9)teseo = new Pair(i,j);
                else if(data==100)minotauro = new Pair(i,j);
                else if(data==1000)salida = new Pair(i,j);
            }
        }
        this.cargarMovimientos();
    }
    
    private void cargarMovimientos(){
        this.movimientos =  new Pair[4];
        this.movimientos[0] = new Pair(-1,0);
        this.movimientos[1] = new Pair(0,-1);
        this.movimientos[2] = new Pair(1, 0);
        this.movimientos[3] = new Pair(0, 1);
    }

    @Override
    public String toString(){
        String msg = new String();
        for(short v[] : laberinto){
            for(short data : v)
                msg+=data+"\t\t";
            msg+="\n";
        }
        return msg;
    }
}
